#!/usr/bin/python

import argparse
import re
import mutagen.mp3
import mutagen.id3
import mutagen.oggvorbis
import mutagen.flac
import mutagen.oggopus
import mutagen.mp4
import mutagen.wavpack

parser = argparse.ArgumentParser()
parser.add_argument("audFile", type=str)
parser.add_argument("rating", type=str)
args = parser.parse_args()

audFile = args.audFile
newRating = args.rating
ratSting = unicode(newRating, "utf-8")

if re.search('\.mp3$', audFile):
    mutFile = mutagen.mp3.MP3(audFile)
    f = mutagen.id3.TXXX(encoding=3, desc=u'Rating', text=[ratSting])
    if mutFile == {}:
        headLessFile = mutagen.id3.ID3FileType(audFile)
        headLessFile.add_tags()
        headLessFile.save(audFile)
        mutFile = mutagen.mp3.MP3(audFile)
    mutFile.tags.setall(u'TXXX:Rating', [f])
elif re.search('\.opus$', audFile):
    mutFile = mutagen.oggopus.OggOpus(audFile)
    mutFile.tags['rating'] = [ratSting]
elif re.search('\.flac$', audFile):
    mutFile = mutagen.flac.FLAC(audFile)
    mutFile.tags['rating'] = [ratSting]
elif re.search('\.ogg$', audFile):
    mutFile = mutagen.oggvorbis.OggVorbis(audFile)
    mutFile.tags['rating'] = [ratSting]
elif re.search('\.wv$', audFile):
    mutFile = mutagen.wavpack.WavPack(audFile)
    mutFile.tags['rating'] = ratSting    
elif re.search('\.mp4$|\.m4a$', audFile):
    mutFile = mutagen.mp4.MP4(audFile)
    if mutFile == {}:
        headLessFile = mutFile
        headLessFile.add_tags()
        headLessFile.save(audFile)
        mutFile = mutagen.mp4.MP4(audFile)
    f = mutagen.mp4.MP4FreeForm(ratSting)
    mutFile.tags['----:com.apple.iTunes:rating'] = [f]
else: print "na"

mutFile.save(audFile)
